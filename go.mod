module gitlab.com/sacules/ms

go 1.13

require (
	github.com/adrg/xdg v0.2.1
	github.com/gdamore/tcell v1.3.0
	github.com/google/go-cmp v0.3.1
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/rivo/tview v0.0.0-20200528200248-fe953220389f
	github.com/stretchr/testify v1.4.0 // indirect
	gitlab.com/sacules/jsonfile v0.2.2
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	golang.org/x/text v0.3.3 // indirect
)
